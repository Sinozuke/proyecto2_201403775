# -*- coding: utf-8 -*-
from channels import Group
from channels.sessions import channel_session
import json

from usac_networking.models import Mensaje

# Connected to websocket.connect
@channel_session
def ws_add(message):
    # Accept the connection
    message.reply_channel.send({"accept": True})

# Connected to websocket.receive
@channel_session
def ws_message(message):
    mensaje = json.loads(message.content['text'])
    if mensaje['tipo'] == 'ingreso':
        message.channel_session["user_id"] = mensaje['user_id']
        Group("chat-%s" % mensaje['user_id']).add(message.reply_channel)
    elif mensaje['tipo'] == 'mensaje':
        mensaje = Mensaje(emisor=mensaje['user_id'],receptor=mensaje['destino'],mensaje=mensaje['text'])
        mensaje.save()
        Group("chat-%s" % mensaje['destino']).send({
            "text": "[user] %s" % message.content['text'],
        })
        Group("chat-%s" % mensaje['user_id']).send({
            "text": "[user] %s" % message.content['text'],
        })

# Connected to websocket.disconnect
@channel_session
def ws_disconnect(message):
    Group("chat-%s" % message.channel_session["user_id"]).discard(message.reply_channel)
