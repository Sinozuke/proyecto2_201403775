import datetime

from django.db import models
from django.utils import timezone
from django.core.files.storage import FileSystemStorage
import os

# Create your models here.

profile_images_dir = FileSystemStorage(location=os.path.join(os.getcwd(),'usac_networking/static/usac_networking/images/user_profile_images'))
themes_images_dir = FileSystemStorage(location=os.path.join(os.getcwd(),'usac_networking/static/usac_networking/images/themes'))

class Facultad(models.Model):
    codigo      = models.IntegerField(unique=True)
    nombre      = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Carrera(models.Model):
    codigo      = models.CharField(max_length=5)
    nombre      = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=300)
    facultad    = models.ForeignKey(Facultad,null=False)
    def __str__(self):
        return self.nombre + ' - \'' + self.facultad.nombre + '\''

class Cargo(models.Model):
    nombre      = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300)
    def __str__(self):
        return self.nombre

class Rol(models.Model):
    nombre      = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre

class Operacion(models.Model):
    nombre      = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Ciencia(models.Model):
    nombre      = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200)
    carrera     = models.ForeignKey(Carrera)
    def __str__(self):
        return self.nombre + ' [ ' + self.carrera.nombre + ' - \'' + self.carrera.facultad.nombre + '\'' + ' ]'

class Estado(models.Model):
    '''
    estado  | significado
    1       | eliminada
    2       | no confirmada
    3       | suspendida
    4       | activa
    '''
    tipo        = models.CharField(max_length=50)
    def __str__(self):
        return self.tipo

class Usuario(models.Model):
    carnet      = models.IntegerField(blank=False,unique=True)
    nombre      = models.CharField(max_length=100)
    apellido    = models.CharField(max_length=100)
    fotografia  = models.ImageField(storage=profile_images_dir)
    correo      = models.EmailField(unique=True)
    telefono    = models.IntegerField(blank=False)
    clave       = models.CharField(max_length=20)
    catedratico = models.BooleanField(default=False)
    estado      = models.ForeignKey(Estado,default=2)
    rol         = models.ForeignKey(Rol,default=2)
    def __str__(self):
        return self.nombre

class Validacion_Usuario(models.Model):

    '''
    estado      |       significado
        0       |       no utlizado
        1       |       utilizado
    '''

    usuario     = models.ForeignKey(Usuario)
    val         = models.CharField(max_length=50)
    fecha       = models.DateTimeField()
    estado      = models.BooleanField(default=False)
    def __str__(self):
        return self.val

class Cargo_Usuario(models.Model):
    cargo       = models.ForeignKey(Cargo)
    Usuario     = models.ForeignKey(Usuario)
    class Meta:
        unique_together = ("cargo","Usuario")

class Asignacion(models.Model):
    carrera     = models.ForeignKey(Carrera)
    usuario     = models.ForeignKey(Usuario)
    fecha       = models.DateField(auto_now_add=True)
    class Meta:
        unique_together = ("carrera","usuario")

class Ciencia_Asignado(models.Model):
    usuario     = models.ForeignKey(Usuario)
    ciencia     = models.ForeignKey(Ciencia)
    class Meta:
        unique_together = ("usuario","ciencia")

class Tema(models.Model):
    '''
    estado | descripccion
    1      | No Solucionado
    2      | Solucionado
    3      | Clausurado
    4      | Eliminado
    '''
    titulo      = models.CharField(max_length=100)
    imagen      = models.ImageField(storage=profile_images_dir,null=True)
    contenido   = models.TextField()
    fecha       = models.DateField(auto_now_add=True)
    estado      = models.IntegerField(default=1)
    usuario     = models.ForeignKey(Usuario)
    comment     = models.IntegerField(default=0)
    def __str__(self):
        return self.titulo

class Bitacora(models.Model):
    usuario_r   = models.ForeignKey(Usuario,null=True, related_name='usuario_afectado')
    tema_r      = models.ForeignKey(Tema,null=True)
    usuario_ad  = models.ForeignKey(Usuario, related_name='usuario_administrador')
    operacion   = models.ForeignKey(Operacion)
    descripcion = models.CharField(max_length=200)
    fecha       = models.DateField(auto_now_add=True)

class Respuesta(models.Model):
    contenido   = models.CharField(max_length=500)
    r_padre     = models.ForeignKey('self',null=True)
    t_padre     = models.ForeignKey(Tema,null=True)
    usuario     = models.ForeignKey(Usuario)
    fecha       = models.DateField(auto_now_add=True)
    num_res     = models.IntegerField(default=0)
    def __str__(self):
        return self.contenido

class Ciencia_rel(models.Model):
    tema        = models.ForeignKey(Tema)
    ciencia     = models.ForeignKey(Ciencia)
    class Meta:
        unique_together = ("tema","ciencia")

class Facultad_rel(models.Model):
    tema        = models.ForeignKey(Tema)
    facultad    = models.ForeignKey(Facultad)
    class Meta:
        unique_together = ("tema","facultad")

class Carrera_rel(models.Model):
    tema        = models.ForeignKey(Tema)
    carrera     = models.ForeignKey(Carrera)
    class Meta:
        unique_together = ("tema","carrera")

class Usuario_inv(models.Model):
    tema        = models.ForeignKey(Tema)
    usuario     = models.ForeignKey(Usuario)
    class Meta:
        unique_together = ("tema","usuario")

class Circulo(models.Model):
    usuario     = models.ForeignKey(Usuario)
    nombre      = models.CharField(max_length=255)
    ciencia     = models.ForeignKey(Ciencia)
    descripcion = models.CharField(max_length=300)
    class Meta:
        unique_together = ("usuario","nombre")

class Circulo_descrp(models.Model):
    circulo     = models.ForeignKey(Circulo)
    usuario     = models.ForeignKey(Usuario)
    estado      = models.IntegerField(default=0)
    class Meta:
        unique_together = ("circulo","usuario")
    def __str__(self):
        return self.circulo.nombre + ' - ' + str(self.usuario.carnet) + ' - ' + str(self.estado)

class Relacion_Amistad(models.Model):
    origen      = models.ForeignKey(Usuario, related_name='usuario_origen')
    relacion    = models.ForeignKey(Usuario, related_name='amistad_del_usuario')
    estado      = models.BooleanField(default=False)
    def __str__(self):
        return '[' + self.origen.nombre + '] - [' + self.relacion.nombre + '] - (' + str(self.estado) + ')'

class Mensaje(models.Model):
    emisor      = models.ForeignKey(Usuario, related_name='usuario_emisor')
    receptor    = models.ForeignKey(Usuario, related_name='usuario_receptor')
    mensaje     = models.CharField(max_length=500)
    fecha       = models.DateTimeField(auto_now_add=True)
    visto       = models.BooleanField(default=False)
    def __str__(self):
        return self.relacion + ' - ' + self.mensaje + ' - ' + str(self.fecha) + '-' + self.visto
