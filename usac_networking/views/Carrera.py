# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import Carrera

def CarreraIndex(request):
    title = 'Usac NetWorking - Carreras'
    err_msg = ''
    msg = ''
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            if request.method == 'POST':
                try:
                    carreraformset = modelformset_factory(Carrera, fields=('codigo','nombre','descripcion','facultad',))
                    if request.POST['c_op'] == 'del':
                        carrera = Carrera.objects.get(pk=request.POST['c_id'])
                        carrera.delete()
                        msg = 'Carrera Eliminana.'
                    elif request.POST['c_op'] == 'upd':
                        carreraform = carreraformset(request.POST,request.FILES)
                        if carreraform.is_valid():
                            carreraform.save()
                            msg = 'Carrera Modificada.'
                        else:
                            err_msg = carreraform.errors
                            return render(request, 'usac_networking/Admin/Carrera/detail.html',{
                                        'title'         : title,
                                        'session'       : request.session['session'],
                                        'formset'       : carreraform,
                                        'err_msg'       : err_msg,
                                        'op'            : 'upd',
                                        })
                    else:
                        carreraform = carreraformset(request.POST,request.FILES)
                        if carreraform.is_valid():
                            carreraform.save()
                            msg = 'Carrera Creada.'
                        else:
                            return HttpResponseRedirect(reverse('usac_networking:fac_nueva'))
                except Carrera.DoesNotExist:
                    raise Http404("Access Denied")
            carreras = Carrera.objects.all()
            return render(request, 'usac_networking/Admin/Carrera/list.html',{
                        'title':title,
                        'session'       : request.session['session'],
                        'carreras'      : carreras,
                        'msg'           : msg,
                        'err_msg'       : err_msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CarreraNueva(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Nueva Carrera'
            err_msg = ''
            msg = ''
            carreraformset = modelformset_factory(Carrera, fields=('codigo','nombre','descripcion','facultad',))
            if request.method == "POST":
                carreraform = carreraformset(request.POST,request.FILES)
                carreraform.is_valid()
                err_msg = carreraform.errors
            else:
                carreraform = carreraformset(queryset=Carrera.objects.none())
            return render(request, 'usac_networking/Admin/Carrera/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : carreraform,
                        'err_msg'       : err_msg,
                        'op'            : 'new'
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CarreraMod(request,c_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Modificar Carrera'
            err_msg = ''
            msg = ''
            carreraformset = modelformset_factory(Carrera, fields=('codigo','nombre','descripcion','facultad',),max_num=1)
            if request.method == "POST":
                carreraform = carreraformset(request.POST,request.FILES)
                if carreraform.is_valid():
                    carreraform.save()
                    msg = 'Carrera Modificada.'
                    carreraes = Carrera.objects.all()
                    return render(request, 'usac_networking/Admin/Carrera/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'carreraes'    : carreraes,
                                'msg'           : msg,
                                'err_msg'       : err_msg
                                })
                else:
                    err_msg = carreraform.errors
            else:
                carreraform = carreraformset(queryset=Carrera.objects.filter(pk=c_id))
            return render(request, 'usac_networking/Admin/Carrera/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : carreraform,
                        'err_msg'       : err_msg,
                        'op'            : 'upd',
                        'c_id'          : int(c_id),
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
