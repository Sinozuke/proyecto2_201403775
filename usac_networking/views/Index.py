# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404

from usac_networking.models import Usuario, Tema, Circulo, Facultad, Ciencia, Carrera

def IndexView(request):
    title = 'Usac NetWorking - index'
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            return HttpResponseRedirect(reverse('usac_networking:Admin_index'))
        if request.session['log_in'] == 2:
            return HttpResponseRedirect(reverse('usac_networking:User_index'))
    request.session['session'] = 0
    temas = Tema.objects.all().reverse()[:5]
    return render(request, 'usac_networking/Frontend/home.html',{
                'title':title,
                'session'   : 0,
                'temas'     :temas
                })

def AdminView(request):
    title = 'Admin - Usac NetWorking'
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            usuarios = Usuario.objects.all().reverse()[:5]
            temas = Tema.objects.all().reverse()[:5]
            return render(request, 'usac_networking/Admin/home.html',{
                        'title':title,
                        'session'   : request.session['session'],
                        'temas'     : temas,
                        'usuarios'  : usuarios,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def UserView(request):
    title = 'User - Usac NetWorking'
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            temas = Tema.objects.filter(usuario__pk=request.session['user_id']).reverse()
            return render(request, 'usac_networking/User/home.html',{
                        'title'     : title,
                        'session'   : request.session['session'],
                        'temas'          : temas,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
