# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms
import re
import string

from usac_networking.models import Ciencia

def CienciaIndex(request):
    title = 'Usac NetWorking - Ciencias'
    err_msg = ''
    msg = ''
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            if request.method == 'POST':
                try:
                    cienciaformset = modelformset_factory(Ciencia, fields=('nombre','descripcion','carrera',))
                    if request.POST['c_op'] == 'del':
                        ciencia = Ciencia.objects.get(pk=request.POST['c_id'])
                        ciencia.delete()
                        msg = 'Ciencia Eliminana.'
                    elif request.POST['c_op'] == 'upd':
                        cienciaform = cienciaformset(request.POST,request.FILES)
                        if cienciaform.is_valid():
                            cienciaform.save()
                            msg = 'Ciencia Modificada.'
                        else:
                            err_msg = cienciaform.errors
                            return render(request, 'usac_networking/Admin/Ciencia/detail.html',{
                                        'title'         : title,
                                        'session'       : request.session['session'],
                                        'formset'       : cienciaform,
                                        'err_msg'       : err_msg,
                                        'op'            : 'upd',
                                        })
                    else:
                        cienciaform = cienciaformset(request.POST,request.FILES)
                        if cienciaform.is_valid():
                            cienciaform.save()
                            msg = 'Ciencia Creada.'
                        else:
                            return HttpResponseRedirect(reverse('usac_networking:fac_nueva'))
                except Ciencia.DoesNotExist:
                    raise Http404("Access Denied")
            ciencias = Ciencia.objects.all()
            return render(request, 'usac_networking/Admin/Ciencia/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'ciencias'      : ciencias,
                        'msg'           : msg,
                        'err_msg'       : err_msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CienciaNueva(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Nueva Ciencia'
            err_msg = ''
            msg = ''
            cienciaformset = modelformset_factory(Ciencia, fields=('nombre','descripcion','carrera',))
            cienciaform = cienciaformset(queryset=Ciencia.objects.none())
            return render(request, 'usac_networking/Admin/Ciencia/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : cienciaform,
                        'err_msg'       : err_msg,
                        'op'            : 'new'
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CienciaMod(request,c_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Modificar Ciencia'
            err_msg = ''
            msg = ''
            cienciaformset = modelformset_factory(Ciencia, fields=('nombre','descripcion','carrera',),max_num=1)
            cienciaform = cienciaformset(queryset=Ciencia.objects.filter(pk=c_id))
            return render(request, 'usac_networking/Admin/Ciencia/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : cienciaform,
                        'err_msg'       : err_msg,
                        'op'            : 'upd',
                        'c_id'          : int(c_id),
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
