# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms
from django.core.mail import send_mail, BadHeaderError
from datetime import datetime, timedelta
import re
import string
import random

from usac_networking.models import Cargo

def CargoIndex(request):
    title = 'Usac NetWorking - Cargos'
    err_msg = ''
    msg = ''
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            if request.method == 'POST':
                try:
                    cargoformset = modelformset_factory(Cargo, fields=('nombre','descripcion',))
                    if request.POST['c_op'] == 'del':
                        cargo = Cargo.objects.get(pk=request.POST['c_id'])
                        cargo.delete()
                        msg = 'Cargo Eliminana.'
                    elif request.POST['c_op'] == 'upd':
                        cargoform = cargoformset(request.POST,request.FILES)
                        if cargoform.is_valid():
                            cargoform.save()
                            msg = 'Cargo Modificada.'
                        else:
                            err_msg = cargoform.errors
                            return render(request, 'usac_networking/Admin/Cargo/detail.html',{
                                        'title'         : title,
                                        'session'       : request.session['session'],
                                        'formset'       : cargoform,
                                        'err_msg'       : err_msg,
                                        'op'            : 'upd',
                                        })
                    else:
                        cargoform = cargoformset(request.POST,request.FILES)
                        if cargoform.is_valid():
                            cargoform.save()
                            msg = 'Cargo Creada.'
                        else:
                            return HttpResponseRedirect(reverse('usac_networking:fac_nueva'))
                except Cargo.DoesNotExist:
                    raise Http404("Access Denied")
            cargos = Cargo.objects.all()
            return render(request, 'usac_networking/Admin/Cargo/list.html',{
                        'title':title,
                        'session'       : request.session['session'],
                        'cargos'      : cargos,
                        'msg'           : msg,
                        'err_msg'       : err_msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CargoNueva(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Nueva Cargo'
            err_msg = ''
            msg = ''
            cargoformset = modelformset_factory(Cargo, fields=('nombre','descripcion',))
            if request.method == "POST":
                cargoform = cargoformset(request.POST,request.FILES)
                cargoform.is_valid()
                err_msg = cargoform.errors
            else:
                cargoform = cargoformset(queryset=Cargo.objects.none())
            return render(request, 'usac_networking/Admin/Cargo/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : cargoform,
                        'err_msg'       : err_msg,
                        'op'            : 'new'
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CargoMod(request,c_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Modificar Cargo'
            err_msg = ''
            msg = ''
            cargoformset = modelformset_factory(Cargo, fields=('nombre','descripcion',),max_num=1)
            if request.method == "POST":
                cargoform = cargoformset(request.POST,request.FILES)
                if cargoform.is_valid():
                    cargoform.save()
                    msg = 'Cargo Modificada.'
                    cargoes = Cargo.objects.all()
                    return render(request, 'usac_networking/Admin/Cargo/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'cargoes'    : cargoes,
                                'msg'           : msg,
                                'err_msg'       : err_msg
                                })
                else:
                    err_msg = cargoform.errors
            else:
                cargoform = cargoformset(queryset=Cargo.objects.filter(pk=c_id))
            return render(request, 'usac_networking/Admin/Cargo/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : cargoform,
                        'err_msg'       : err_msg,
                        'op'            : 'upd',
                        'c_id'          : int(c_id),
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
