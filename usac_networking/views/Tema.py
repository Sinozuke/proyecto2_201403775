# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import Tema, Ciencia_rel, Facultad_rel, Carrera_rel, Usuario_inv, Respuesta

def TemaIndex(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Mis Temas'
            msg = ''
            if request.method == 'POST':
                if request.POST.get('f_op') is not None:
                    if request.POST['f_op'] == 'del':
                        tema = Tema.objects.get(pk=request.POST['f_id'])
                        tema.delete()
                        msg = 'Tema Eliminado con Exito'
            temas = Tema.objects.filter(usuario__pk=request.session['user_id'])
            invitaciones = Usuario_inv.objects.filter(usuario__pk=request.session['user_id'])
            return render(request, 'usac_networking/User/Tema/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'temas'         : temas,
                        'temas_inv'     : invitaciones,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAdminIndex(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Temas'
            msg = ''
            if request.method == 'POST':
                if request.POST.get('f_op') is not None:
                    if request.POST['f_op'] == 'del':
                        tema = Tema.objects.get(pk=request.POST['f_id'])
                        tema.delete()
                        msg = 'Tema Eliminado con Exito'
            temas = Tema.objects.all()
            return render(request, 'usac_networking/Admin/Tema/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'temas'         : temas,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaNew(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Nuevo Tema'
            err_msg = '';
            temaformset = modelformset_factory(Tema, exclude=({'estado','comment',}))
            if request.method == "POST":
                temaform = temaformset(request.POST,request.FILES)
                if temaform.is_valid():
                    temasaved = temaform.save()
                    msg = 'Tema creado con Exito'

                    ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'))
                    facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'))
                    carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'))
                    usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'))

                    ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':temasaved[0].pk,}])
                    facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':temasaved[0].pk,}])
                    carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':temasaved[0].pk,}])
                    usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':temasaved[0].pk,}])

                    return render(request, 'usac_networking/User/Tema/related.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'cienciaform'   : ciencia_relform,
                                'facultadform'  : facultad_relform,
                                'carreraform'   : carrera_relform,
                                'usuarioform'   : usuario_invform,
                                't_id'          : temasaved[0].pk,
                                'msg'           : msg,
                                })
                else:
                    err_msg = temaform.errors
            else:
                temaform = temaformset(queryset=Tema.objects.none(),initial=[{'usuario':request.session['user_id'],}])
            return render(request, 'usac_networking/User/Tema/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : temaform,
                        'err_msg'       : err_msg,
                        'op'            : 'new',
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAdd(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Añadir relaciones a tema'
            err_msg = ''
            msg = ''

            ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'),max_num=1)
            facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'),max_num=1)
            carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'),max_num=1)
            usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'),max_num=1)

            ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':t_id,}])
            facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':t_id,}])
            carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':t_id,}])
            usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':t_id,}])

            ciencia_rel = Ciencia_rel.objects.filter(tema=t_id)
            facultad_rel = Facultad_rel.objects.filter(tema=t_id)
            carrera_rel = Carrera_rel.objects.filter(tema=t_id)
            usuario_inv = Usuario_inv.objects.filter(tema=t_id)

            return render(request, 'usac_networking/User/Tema/related.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'cienciaform'   : ciencia_relform,
                        'facultadform'  : facultad_relform,
                        'carreraform'   : carrera_relform,
                        'usuarioform'   : usuario_invform,
                        'ciencias'      : ciencia_rel,
                        'facultades'    : facultad_rel,
                        'carreras'      : carrera_rel,
                        'usuarios_inv'   : usuario_inv,
                        't_id'       : t_id,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAddCiencia(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Añadir relaciones a tema'
            err_msg = ''
            msg = ''

            ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'))
            facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'))
            carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'))
            usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'))

            if request.method == "POST":
                ciencia_relform = ciencia_relformset(request.POST,request.FILES)
                if ciencia_relform.is_valid():
                    ciencia_relform.save()
                    msg = 'Se ha añadido la ciencia con exito'
                else:
                    err_msg = ciencia_relform.errors
            else:
                ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':t_id,}])

            facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':t_id,}])
            carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':t_id,}])
            usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':t_id,}])

            ciencia_rel = Ciencia_rel.objects.filter(tema=t_id)
            facultad_rel = Facultad_rel.objects.filter(tema=t_id)
            carrera_rel = Carrera_rel.objects.filter(tema=t_id)
            usuario_inv = Usuario_inv.objects.filter(tema=t_id)

            return render(request, 'usac_networking/User/Tema/related.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'cienciaform'   : ciencia_relform,
                        'facultadform'  : facultad_relform,
                        'carreraform'   : carrera_relform,
                        'usuarioform'   : usuario_invform,
                        'ciencias'      : ciencia_rel,
                        'facultades'    : facultad_rel,
                        'carreras'      : carrera_rel,
                        'usuarios_inv'   : usuario_inv,
                        't_id'          : t_id,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAddFacultad(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Añadir relaciones a tema'
            err_msg = ''
            msg = ''

            ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'))
            facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'))
            carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'))
            usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'))

            if request.method == "POST":
                facultad_relform = facultad_relformset(request.POST,request.FILES)
                if facultad_relform.is_valid():
                    facultad_relform.save()
                    msg = 'Se ha añadido la facultad con exito'
                else:
                    err_msg = facultad_relform.errors
            else:
                facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':t_id,}])

            ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':t_id,}])
            carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':t_id,}])
            usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':t_id,}])

            ciencia_rel = Ciencia_rel.objects.filter(tema=t_id)
            facultad_rel = Facultad_rel.objects.filter(tema=t_id)
            carrera_rel = Carrera_rel.objects.filter(tema=t_id)
            usuario_inv = Usuario_inv.objects.filter(tema=t_id)

            return render(request, 'usac_networking/User/Tema/related.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'cienciaform'   : ciencia_relform,
                        'facultadform'  : facultad_relform,
                        'carreraform'   : carrera_relform,
                        'usuarioform'   : usuario_invform,
                        'ciencias'      : ciencia_rel,
                        'facultades'    : facultad_rel,
                        'carreras'      : carrera_rel,
                        'usuarios_inv'   : usuario_inv,
                        't_id'          : t_id,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAddCarrera(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Añadir relaciones a tema'
            err_msg = ''
            msg = ''

            ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'))
            facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'))
            carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'))
            usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'))

            if request.method == "POST":
                carrera_relform = carrera_relformset(request.POST,request.FILES)
                if carrera_relform.is_valid():
                    carrera_relform.save()
                    msg = 'Se ha añadido la carrera con exito'
                else:
                    err_msg = carrera_relform.errors
            else:
                carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':t_id,}])

            ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':t_id,}])
            usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':t_id,}])
            facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':t_id,}])

            ciencia_rel = Ciencia_rel.objects.filter(tema=t_id)
            facultad_rel = Facultad_rel.objects.filter(tema=t_id)
            carrera_rel = Carrera_rel.objects.filter(tema=t_id)
            usuario_inv = Usuario_inv.objects.filter(tema=t_id)

            return render(request, 'usac_networking/User/Tema/related.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'cienciaform'   : ciencia_relform,
                        'facultadform'  : facultad_relform,
                        'carreraform'   : carrera_relform,
                        'usuarioform'   : usuario_invform,
                        'ciencias'      : ciencia_rel,
                        'facultades'    : facultad_rel,
                        'carreras'      : carrera_rel,
                        'usuarios_inv'   : usuario_inv,
                        't_id'          : t_id,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaAddUsuario(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Añadir relaciones a tema'
            err_msg = ''
            msg = ''

            ciencia_relformset = modelformset_factory(Ciencia_rel, fields=('tema','ciencia'))
            facultad_relformset = modelformset_factory(Facultad_rel, fields=('tema','facultad'))
            carrera_relformset = modelformset_factory(Carrera_rel, fields=('tema','carrera'))
            usuario_invformset = modelformset_factory(Usuario_inv, fields=('tema','usuario'))

            usuario_invform = usuario_invformset(request.POST,request.FILES)
            if request.method == 'POST':
                if usuario_invform.is_valid():
                    usuario_invform.save()
                    msg = 'Se ha añadido al usuario con exito'
                else:
                    err_msg = usuario_invform.errors
            else:
                usuario_invform = usuario_invformset(queryset=Usuario_inv.objects.none(),initial=[{'tema':t_id,}])

            ciencia_relform = ciencia_relformset(queryset=Ciencia_rel.objects.none(),initial=[{'tema':t_id,}])
            facultad_relform = facultad_relformset(queryset=Facultad_rel.objects.none(),initial=[{'tema':t_id,}])
            carrera_relform = carrera_relformset(queryset=Carrera_rel.objects.none(),initial=[{'tema':t_id,}])

            ciencia_rel = Ciencia_rel.objects.filter(tema=t_id)
            facultad_rel = Facultad_rel.objects.filter(tema=t_id)
            carrera_rel = Carrera_rel.objects.filter(tema=t_id)
            usuario_inv = Usuario_inv.objects.filter(tema=t_id)

            return render(request, 'usac_networking/User/Tema/related.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'cienciaform'   : ciencia_relform,
                        'facultadform'  : facultad_relform,
                        'carreraform'   : carrera_relform,
                        'usuarioform'   : usuario_invform,
                        'ciencias'      : ciencia_rel,
                        'facultades'    : facultad_rel,
                        'carreras'      : carrera_rel,
                        'usuarios_inv'   : usuario_inv,
                        't_id'          : t_id,
                        'msg'           : msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaUpdate(request,t_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Modificar Tema'
            err_msg = '';
            temaformset = modelformset_factory(Tema, exclude=({'estado','comment',}),max_num=1)
            if request.method == "POST":
                temaform = temaformset(request.POST,request.FILES)
                if temaform.is_valid():
                    temaform.save()
                    msg = 'Tema modificado con exito'
                    temas = Tema.objects.filter(usuario__pk=request.session['user_id'])
                    invitaciones = Usuario_inv.objects.filter(usuario__pk=request.session['user_id'])
                    return render(request, 'usac_networking/User/Tema/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'temas'         : temas,
                                'temas_inv'     : invitaciones,
                                'msg'           : msg,
                                })
                else:
                    err_msg = temaform.errors
            else:
                temaform = temaformset(queryset=Tema.objects.filter(pk=t_id),initial=[{'usuario':request.session['user_id'],}])
            return render(request, 'usac_networking/User/Tema/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : temaform,
                        'err_msg'       : err_msg,
                        'op'            : 'update',
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def TemaInfo(request,t_id):
    tema = Tema.objects.get(pk=t_id)

    ciencias_rel    = Ciencia_rel.objects.filter(tema__pk=t_id)
    facultades_rel  = Facultad_rel.objects.filter(tema__pk=t_id)
    carreras_rel    = Carrera_rel.objects.filter(tema__pk=t_id)
    usuarios_inv    = Usuario_inv.objects.filter(tema__pk=t_id)
    comentarios     = Respuesta.objects.filter(t_padre__pk=t_id)

    if request.session.get('log_in'):
        editable_comment = False;
        if tema.usuario.pk == request.session['user_id'] or request.session['log_in'] == 1 :
            editable = True
            editable_comment = True;
        else:
            editable = False

        return render(request, 'usac_networking/User/Tema/info.html',{
                    'title'         : tema.titulo + ' - Usac NetWorking',
                    'session'       : request.session['session'],
                    'tema'          : tema,
                    'ciencias'      : ciencias_rel,
                    'facultades'    : facultades_rel,
                    'carreras'      : carreras_rel,
                    'usuarios'      : usuarios_inv,
                    'editable'      : editable,
                    'comment_editable': editable_comment,
                    'u_id'          : request.session['user_id'],
                    'comentarios'   : comentarios,
                    'comment'       : True,
                    })
    return render(request, 'usac_networking/User/Tema/info.html',{
                'title'         : tema.titulo + ' - Usac NetWorking',
                'session'       : 0,
                'tema'          : tema,
                'ciencias'      : ciencias_rel,
                'facultades'    : facultades_rel,
                'carreras'      : carreras_rel,
                'usuarios'      : usuarios_inv,
                'editable'      : False,
                'comment_editable': False,
                'u_id'          : -1,
                'comentarios'   : comentarios,
                'comment'       : False,
                })
