# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import Usuario, Circulo, Usuario_inv, Circulo_descrp

def CirculoIndex(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Mis Circulos'
            if request.method == 'POST':
                circulo_del = Circulo.objects.get(pk=request.POST['f_id'])
                circulo_del.delete()
            circulos = Circulo.objects.filter(usuario__pk=request.session['user_id'])
            notificaciones = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=0)
            participante = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=1)
            return render(request, 'usac_networking/User/Circulo/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'circulos'      : circulos,
                        'circulosinv'   : participante,
                        'notificaciones': notificaciones,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CirculoAdminIndex(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Circulos'
            if request.method == 'POST':
                circulo_del = Circulo.objects.get(pk=request.POST['f_id'])
                circulo_del.delete()
            circulos = Circulo.objects.all()
            return render(request, 'usac_networking/Admin/Circulo/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'circulos'      : circulos,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CirculoNew(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Nuevo Circulo'
            err_msg = '';
            circuloformset = modelformset_factory(Circulo, fields=('usuario','nombre','ciencia','descripcion',))
            if request.method == "POST":
                circuloform = circuloformset(request.POST,request.FILES)
                if circuloform.is_valid():
                    circuloform.save()
                    msg = 'Circulo creado con Exito'
                    circulos = Circulo.objects.filter(usuario__pk=request.session['user_id'])
                    notificaciones = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=0)
                    participante = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=1)
                    return render(request, 'usac_networking/User/Circulo/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'circulos'      : circulos,
                                'circulosinv'   : participante,
                                'notificaciones': notificaciones,
                                'msg'           : msg,
                                })
                else:
                    err_msg = circuloform.errors
            else:
                circuloform = circuloformset(queryset=Circulo.objects.none(),initial=[{'usuario':request.session['user_id'],}])
            return render(request, 'usac_networking/User/Circulo/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : circuloform,
                        'err_msg'       : err_msg,
                        'op'            : 'new',
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def CirculoAdd(request,c_id):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Añadir Contactos'
        err_msg = ''
        msg = ''
        circuloformset = modelformset_factory(Circulo_descrp, fields=('circulo','usuario',))
        if request.method == "POST":
            circuloform = circuloformset(request.POST,request.FILES)
            if circuloform.is_valid():
                circuloform.save()
                msg = 'Se ha mandado solicitud de pertenencia al circulo con Exito'
            else:
                err_msg = circuloform.errors
        else:
            circuloform = circuloformset(queryset=Circulo_descrp.objects.none(),initial=[{'circulo':c_id,}])
        circulo = Circulo_descrp.objects.filter(circulo=c_id)
        return render(request, 'usac_networking/User/Circulo/add.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'formset'       : circuloform,
                    'err_msg'       : err_msg,
                    'msg'           : msg,
                    'c_id'          : c_id,
                    'circulod'      : circulo,
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))


def CirculoUpdate(request,c_id):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Modificar Circulo'
        err_msg = '';
        circuloformset = modelformset_factory(Circulo, fields=('usuario','nombre','ciencia','descripcion',),max_num=1)
        if request.method == "POST":
            circuloform = circuloformset(request.POST,request.FILES)
            if circuloform.is_valid():
                circuloform.save()
                msg = 'Circulo modificado con exito'
                circulos = Circulo.objects.filter(usuario__pk=request.session['user_id'])
                notificaciones = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=0)
                participante = Circulo_descrp.objects.filter(usuario__pk=request.session['user_id'],estado=1)
                return render(request, 'usac_networking/User/Circulo/list.html',{
                            'title'         : title,
                            'session'       : request.session['session'],
                            'circulos'      : circulos,
                            'circulosinv'   : participante,
                            'notificaciones': notificaciones,
                            'msg'           : msg,
                            })
            else:
                err_msg = circuloform.errors
        else:
            circuloform = circuloformset(queryset=Circulo.objects.filter(pk=c_id),initial=[{'usuario':request.session['user_id']}])
        return render(request, 'usac_networking/User/Circulo/detail.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'formset'       : circuloform,
                    'err_msg'       : err_msg,
                    'op'            : 'update',
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def Circuloinfo(request,c_id):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Info Circulo'
        msg = ''
        if request.method == 'POST':
            Circulo_descrp.objects.filter(circulo=c_id,usuario=request.POST['f_id']).delete()
            msg = 'Usuario removido'
        circulo = Circulo.objects.get(pk=c_id)
        if circulo.usuario.pk == request.session['user_id'] or request.session['log_in'] == 1:
            editable = True
        else:
            editable = False
        circulo_des = Circulo_descrp.objects.filter(circulo__pk=c_id)
        return render(request, 'usac_networking/User/Circulo/info.html',{
                    'title'         : title,
                    'msg'           : msg,
                    'session'       : request.session['session'],
                    'circulo'       : circulo,
                    'circulo_des'   : circulo_des,
                    'editable'      : editable
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))
