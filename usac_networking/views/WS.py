# -*- coding: utf-8 -*-
from django.http import JsonResponse, Http404
from django.shortcuts import render
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

from usac_networking.models import *

def CirculoNo(request):
    if request.GET.get('op') is not None:
        if request.GET.get('id') is not None:
            try:
                solicitud = Circulo_descrp.objects.get(pk=request.GET['id'])
                if request.GET['op'] == 'aceptar':
                    solicitud.estado = 1
                    solicitud.save()
                elif request.GET['op'] == 'salir':
                    solicitud.delete()
                else:
                    solicitud.delete()
                return JsonResponse({'result' : 'OK'})
            except Circulo_descrp.DoesNotExist():
                pass
    raise Http404("Access Denied")

def WSTemas(request):
    data = serializers.serialize("json", Tema.objects.all())
    return JsonResponse({'data':data})

@csrf_exempt
def WSLogin(request):
    if request.method == 'POST':
        if request.POST.get('username') is not None and request.POST.get('password') is not None:
            try:
                usuario = Usuario.objects.get(carnet=request.POST['username'])
                if usuario.rol.pk == 1:
                    return JsonResponse({
                                            'status'    :"fail",
                                            'msg'       :"Administradores no Pueden Acceder a Esta Plataforma"
                                        })
                else:
                    if usuario.clave == request.POST['password']:
                        return JsonResponse({
                                                'status'    :"ok",
                                                'user_id'   :usuario.pk
                                            })
                    else:
                        return JsonResponse({
                                                'status'    :"fail",
                                                'msg'       :"Contraseña Incorrecta"
                                            })
            except Usuario.DoesNotExist:
                return JsonResponse({
                                        'status'    :"fail",
                                        'msg'       :"No Existe El Usuario"
                                    })
    return JsonResponse({
                            'status'    :"fail",
                            'msg'       :"Access Denied"
                        })

def WSAmistad(request):
    if request.GET.get('op') is not None:
        if request.GET['op'] == 'agregar':
            if request.GET.get('u1_id') is not None:
                if request.GET.get('u2_id') is not None:
                    relacion = Relacion_Amistad(
                        origen = Usuario.objects.get(pk=request.GET['u1_id']),
                        relacion = Usuario.objects.get(pk=request.GET['u2_id'])
                    )
                    relacion.save()
        elif request.GET['op'] == 'aceptar':
            if request.GET.get('id') is not None:
                relacion = Relacion_Amistad.objects.get(
                    pk=request.GET['id']
                );
                relacion.estado = True
                relacion.save()
        elif request.GET['op'] == 'cancelar':
            if request.GET.get('u1_id') is not None:
                if request.GET.get('u2_id') is not None:
                    relacion = Relacion_Amistad.objects.get(
                        origen__pk=request.GET['u1_id'],
                        relacion__pk=request.GET['u2_id'],
                    );
                    relacion.delete()
        elif request.GET['op'] == 'rechazar':
            if request.GET.get('id') is not None:
                relacion = Relacion_Amistad.objects.get(
                    pk=request.GET['id']
                );
                relacion.delete()
        else:
            if request.GET.get('id') is not None:
                relacion = Relacion_Amistad.objects.get(
                    pk=request.GET['id']
                );
                relacion.delete()
        return JsonResponse({'result' : 'OK'})
    raise Http404("Access Denied")

def CommentDel(request):
    if request.GET.get('id') is not None:
        if request.GET.get('padre') is not None:
            if request.GET.get('tipo') is not None:
                if request.GET['tipo'] == 'tema':
                    tema = Tema.objects.get(pk=request.GET['padre'])
                    tema.comment -= 1
                    tema.save()
                else:
                    respuesta = Respuesta.objects.get(pk=request.GET['padre'])
                    respuesta.num_res -= 1
                    respuesta.save()
                respuesta = Respuesta.objects.get(pk=request.GET['id'])
                respuesta.delete()
                return JsonResponse({'result' : 'OK'})
    raise Http404("Access Denied")

def getComments(request):
    if request.GET.get('padre') is not None:
        respuestas = Respuesta.objects.filter(r_padre=request.GET['padre'])
        usuarios = list(Usuario.objects.none())
        for respuesta in respuestas:
            usuarios += list(Usuario.objects.filter(pk=respuesta.usuario.pk))
        data = serializers.serialize("json", respuestas)
        users = serializers.serialize("json", usuarios)
        return JsonResponse({'coments' : data,'usuarios':users,'num':len(respuestas)})
    raise Http404("Access Denied")

def Comment(request):
    if request.GET.get('padre') is not None:
        if request.GET.get('tipo') is not None:
            if request.GET.get('contenido') is not None:
                if request.GET.get('user_id') is not None:
                    respuesta = Respuesta(
                                    contenido=request.GET['contenido'],
                                    usuario=Usuario.objects.get(pk=request.GET['user_id']))
                    if request.GET['tipo'] == 'tema':
                        tema_padre = Tema.objects.get(pk=request.GET['padre'])
                        tema_padre.comment += 1
                        tema_padre.save()
                        respuesta.t_padre = Tema.objects.get(pk=request.GET['padre'])
                    else:
                        res_padre = Respuesta.objects.get(pk=request.GET['padre'])
                        res_padre.num_res += 1
                        res_padre.save()
                        respuesta.r_padre = Respuesta.objects.get(pk=request.GET['padre'])
                    respuesta.save()
                    data = serializers.serialize("json", Respuesta.objects.filter(pk=respuesta.pk))
                    user = serializers.serialize("json", Usuario.objects.filter(pk=respuesta.usuario.pk))
                    return JsonResponse({'result' : data,'usuario':user})
    raise Http404("Access Denied")

def TemaNo(request):
    if request.GET.get('op') is not None:
        if request.GET.get('id') is not None:
            try:
                tema_inv = Usuario_inv.objects.get(pk=request.GET['id'])
                tema_inv.delete()
                return JsonResponse({'result' : 'OK'})
            except Circulo_descrp.DoesNotExist():
                pass
    raise Http404("Access Denied")

def WSUsuarios(request):
    data = serializers.serialize("json", Usuario.objects.all())
    return JsonResponse({'result' : data})

def WSFacultades(request):
    data = serializers.serialize("json", Facultad.objects.all())
    return JsonResponse({'result' : data})

def WSUsuario(request):
    data = serializers.serialize("json", Usuario.objects.filter(pk=request.GET['uid']))
    return JsonResponse({'result' : data})

def WSBusqueda(request):
    if request.GET.get('search') is None and request.POST.get('search') is None:
        raise Http404("Access Denied")

    if request.method == 'POST':
        str_busqueda = request.POST['search']
    else:
        str_busqueda = request.GET['search']
    lst_busqueda = str_busqueda.split(' ')
    usuarios    = list(Usuario.objects.none())
    temas       = list(Tema.objects.none())
    circulos    = list(Circulo.objects.none())
    for palabra in lst_busqueda:

        #Usuarios
        usuarios +=  list(Usuario.objects.filter(nombre__icontains=palabra))
        usuarios +=  list(Usuario.objects.filter(apellido__icontains=palabra))
        usuarios +=  list(Usuario.objects.filter(correo__icontains=palabra))

        #Temas
        temas +=  list(Tema.objects.filter(titulo__icontains=palabra))
        temas +=  list(Tema.objects.filter(usuario__nombre__icontains=palabra))

        circulos += list(Circulo.objects.filter(nombre__icontains=palabra))
        circulos += list(Circulo.objects.filter(usuario__nombre__icontains=palabra))

    users_set       = set(usuarios)
    temas_set       = set(temas)
    circulos_set    = set(circulos)

    if request.method == 'POST':
        return render(request, 'usac_networking/public/search.html',{
                    'title'         : 'Usac NetWorking - Busqueda \"' + str_busqueda + '\"',
                    'session'       : request.session['session'],
                    'query'         : str_busqueda,
                    'usuarios'      : users_set,
                    'temas'         : temas_set,
                    'circulos'      : circulos_set,
                    })

    usuarios_data = serializers.serialize("json", users_set)
    temas_data = serializers.serialize("json", temas_set)
    data = json.dumps({'usuarios':usuarios_data,'temas':temas_data,})
    return JsonResponse({'result' : data})
