# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import Ciencia_Asignado, Cargo_Usuario, Usuario, Asignacion

def AsignacionIndex(request):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Asignaciones'

        cargo_relformset = modelformset_factory(Cargo_Usuario, fields=('cargo','Usuario'),max_num=1)
        asignacion_relformset = modelformset_factory(Asignacion, fields=('carrera','usuario'),max_num=1)
        ciencia_relformset = modelformset_factory(Ciencia_Asignado, fields=('usuario','ciencia'),max_num=1)

        cargo_relform = cargo_relformset(queryset=Cargo_Usuario.objects.none(),initial=[{'Usuario':request.session['user_id'],}])
        asignacion_relform = asignacion_relformset(queryset=Asignacion.objects.none(),initial=[{'usuario':request.session['user_id'],}])
        ciencia_relform = ciencia_relformset(queryset=Ciencia_Asignado.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargos = Cargo_Usuario.objects.filter(Usuario=request.session['user_id'])
        asignaciones = Asignacion.objects.filter(usuario=request.session['user_id'])
        ciencias = Ciencia_Asignado.objects.filter(usuario=request.session['user_id'])

        return render(request, 'usac_networking/User/Asignacion/related.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'cargoform'     : cargo_relform,
                    'asigform'      : asignacion_relform,
                    'cienciaform'   : ciencia_relform,
                    'cargos'        : cargos,
                    'asignaciones'  : asignaciones,
                    'ciencias'      : ciencias,
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def AsignacionCargo(request):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Asignaciones'
        err_msg = ''
        msg = ''

        cargo_relformset = modelformset_factory(Cargo_Usuario, fields=('cargo','Usuario'),max_num=1)
        asignacion_relformset = modelformset_factory(Asignacion, fields=('carrera','usuario'),max_num=1)
        ciencia_relformset = modelformset_factory(Ciencia_Asignado, fields=('usuario','ciencia'),max_num=1)

        if request.method == "POST":
            cargo_relform = cargo_relformset(request.POST,request.FILES)
            if cargo_relform.is_valid():
                cargo_relform.save()
                msg = 'Se ha añadido el cargo con exito'
            else:
                err_msg = cargo_relform.errors
        else:
            cargo_relform = cargo_relformset(queryset=Cargo_Usuario.objects.none(),initial=[{'Usuario':request.session['user_id'],}])

        asignacion_relform = asignacion_relformset(queryset=Asignacion.objects.none(),initial=[{'usuario':request.session['user_id'],}])
        ciencia_relform = ciencia_relformset(queryset=Ciencia_Asignado.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargos = Cargo_Usuario.objects.filter(Usuario=request.session['user_id'])
        asignaciones = Asignacion.objects.filter(usuario=request.session['user_id'])
        ciencias = Ciencia_Asignado.objects.filter(usuario=request.session['user_id'])

        return render(request, 'usac_networking/User/Asignacion/related.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'cargoform'     : cargo_relform,
                    'asigform'      : asignacion_relform,
                    'cienciaform'   : ciencia_relform,
                    'cargos'        : cargos,
                    'asignaciones'  : asignaciones,
                    'ciencias'      : ciencias,
                    'msg'           : msg,
                    'err_msg'       : err_msg,
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def AsignacionAsignacion(request):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Asignaciones'
        err_msg = ''
        msg = ''

        cargo_relformset = modelformset_factory(Cargo_Usuario, fields=('cargo','Usuario'),max_num=1)
        asignacion_relformset = modelformset_factory(Asignacion, fields=('carrera','usuario'),max_num=1)
        ciencia_relformset = modelformset_factory(Ciencia_Asignado, fields=('usuario','ciencia'),max_num=1)

        if request.method == "POST":
            asignacion_relform = asignacion_relformset(request.POST,request.FILES)
            if asignacion_relform.is_valid():
                asignacion_relform.save()
                msg = 'Asignacion realizada con exito'
            else:
                err_msg = asignacion_relform.errors
        else:
            asignacion_relform = asignacion_relformset(queryset=Asignacion.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargo_relform = cargo_relformset(queryset=Cargo_Usuario.objects.none(),initial=[{'Usuario':request.session['user_id'],}])
        ciencia_relform = ciencia_relformset(queryset=Ciencia_Asignado.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargos = Cargo_Usuario.objects.filter(Usuario=request.session['user_id'])
        asignaciones = Asignacion.objects.filter(usuario=request.session['user_id'])
        ciencias = Ciencia_Asignado.objects.filter(usuario=request.session['user_id'])

        return render(request, 'usac_networking/User/Asignacion/related.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'cargoform'     : cargo_relform,
                    'asigform'      : asignacion_relform,
                    'cienciaform'   : ciencia_relform,
                    'cargos'        : cargos,
                    'asignaciones'  : asignaciones,
                    'ciencias'      : ciencias,
                    'msg'           : msg,
                    'err_msg'       : err_msg,
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def AsignacionCiencia(request):
    if request.session.get('log_in'):
        title = 'Usac NetWorking - Asignaciones'
        err_msg = ''
        msg = ''

        cargo_relformset = modelformset_factory(Cargo_Usuario, fields=('cargo','Usuario'),max_num=1)
        asignacion_relformset = modelformset_factory(Asignacion, fields=('carrera','usuario'),max_num=1)
        ciencia_relformset = modelformset_factory(Ciencia_Asignado, fields=('usuario','ciencia'),max_num=1)

        if request.method == "POST":
            ciencia_relform = ciencia_relformset(request.POST,request.FILES)
            if ciencia_relform.is_valid():
                ciencia_relform.save()
                msg = 'Ciencia Agregada con exito'
            else:
                err_msg = ciencia_relform.errors
        else:
            ciencia_relform = ciencia_relformset(queryset=Ciencia_Asignado.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargo_relform = cargo_relformset(queryset=Cargo_Usuario.objects.none(),initial=[{'Usuario':request.session['user_id'],}])
        asignacion_relform = asignacion_relformset(queryset=Asignacion.objects.none(),initial=[{'usuario':request.session['user_id'],}])

        cargos = Cargo_Usuario.objects.filter(Usuario=request.session['user_id'])
        asignaciones = Asignacion.objects.filter(usuario=request.session['user_id'])
        ciencias = Ciencia_Asignado.objects.filter(usuario=request.session['user_id'])

        return render(request, 'usac_networking/User/Asignacion/related.html',{
                    'title'         : title,
                    'session'       : request.session['session'],
                    'cargoform'     : cargo_relform,
                    'asigform'      : asignacion_relform,
                    'cienciaform'   : ciencia_relform,
                    'cargos'        : cargos,
                    'asignaciones'  : asignaciones,
                    'ciencias'      : ciencias,
                    'msg'           : msg,
                    'err_msg'       : err_msg,
                    })
    return HttpResponseRedirect(reverse('usac_networking:index'))
