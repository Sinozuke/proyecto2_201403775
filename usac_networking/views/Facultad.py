# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404

from usac_networking.models import Facultad

def FacultadIndex(request):
    title = 'Usac NetWorking - Facultades'
    err_msg = ''
    msg = ''
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            if request.method == 'POST':
                try:
                    facultadformset = modelformset_factory(Facultad, fields=('codigo','nombre','descripcion',))
                    if request.POST['f_op'] == 'del':
                        facultad = Facultad.objects.get(pk=request.POST['f_id'])
                        facultad.delete()
                        msg = 'Facultad Eliminana.'
                    elif request.POST['f_op'] == 'upd':
                        facultadform = facultadformset(request.POST,request.FILES)
                        if facultadform.is_valid():
                            facultadform.save()
                            msg = 'Facultad Modificada.'
                        else:
                            err_msg = facultadform.errors
                            return render(request, 'usac_networking/Admin/Facultad/detail.html',{
                                        'title'         : title,
                                        'session'       : request.session['session'],
                                        'formset'       : facultadform,
                                        'err_msg'       : err_msg,
                                        'op'            : 'upd',
                                        'f_id'          : request.POST['form-0-id'],
                                        })
                    else:
                        facultadform = facultadformset(request.POST,request.FILES)
                        if facultadform.is_valid():
                            facultadform.save()
                            msg = 'Facultad Creada.'
                        else:
                            return HttpResponseRedirect(reverse('usac_networking:fac_nueva'))
                except Facultad.DoesNotExist:
                    raise Http404("Access Denied")
            facultades = Facultad.objects.all()
            return render(request, 'usac_networking/Admin/Facultad/list.html',{
                        'title':title,
                        'session'       : request.session['session'],
                        'facultades'    : facultades,
                        'msg'           : msg,
                        'err_msg'       : err_msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def FacultadNueva(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Nueva Facultad'
            err_msg = ''
            msg = ''
            facultadformset = modelformset_factory(Facultad, fields=('codigo','nombre','descripcion',))
            if request.method == "POST":
                facultadform = facultadformset(request.POST,request.FILES)
                facultadform.is_valid()
                err_msg = facultadform.errors
            else:
                facultadform = facultadformset(queryset=Facultad.objects.none())
            return render(request, 'usac_networking/Admin/Facultad/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : facultadform,
                        'err_msg'       : err_msg,
                        'op'            : 'new'
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def FacultadMod(request,f_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Modificar Facultad'
            err_msg = ''
            msg = ''
            facultadformset = modelformset_factory(Facultad, fields=('codigo','nombre','descripcion',),max_num=1)
            if request.method == "POST":
                facultadform = facultadformset(request.POST,request.FILES)
                if facultadform.is_valid():
                    facultadform.save()
                    msg = 'Facultad Modificada.'
                    facultades = Facultad.objects.all()
                    return render(request, 'usac_networking/Admin/Facultad/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'facultades'    : facultades,
                                'msg'           : msg,
                                'err_msg'       : err_msg
                                })
                else:
                    err_msg = facultadform.errors
            else:
                facultadform = facultadformset(queryset=Facultad.objects.filter(pk=f_id))
            return render(request, 'usac_networking/Admin/Facultad/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : facultadform,
                        'err_msg'       : err_msg,
                        'op'            : 'upd',
                        'f_id'          : int(f_id),
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
