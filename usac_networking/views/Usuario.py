# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import *

def UsuarioIndex(request):
    title = 'Usac NetWorking - Usuarios'
    err_msg = ''
    msg = ''
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            if request.method == 'POST':
                try:
                    usuarioformset = modelformset_factory(Usuario, exclude=('correo',))
                    if request.POST['f_op'] == 'del':
                        usuario = Usuario.objects.get(pk=request.POST['u_id'])
                        usuario.estado = Estado.objects.get(pk=1)
                        usuario.save()
                        msg = 'Usuario Eliminana.'
                    elif request.POST['f_op'] == 'upd':
                        usuarioform = usuarioformset(request.POST,request.FILES)
                        if usuarioform.is_valid():
                            usuarioform.save()
                            msg = 'Usuario Modificada.'
                        else:
                            err_msg = usuarioform.errors
                            return render(request, 'usac_networking/Admin/Usuario/detail.html',{
                                        'title'         : title,
                                        'session'       : request.session['session'],
                                        'formset'       : usuarioform,
                                        'err_msg'       : err_msg,
                                        'op'            : 'upd',
                                        'u_id'          : request.POST['form-0-id'],
                                        })
                except Usuario.DoesNotExist:
                    raise Http404("Access Denied")
            usuarios = Usuario.objects.filter(catedratico=False,rol=2)
            administradores = Usuario.objects.filter(rol=1)
            catedraticos = Usuario.objects.filter(catedratico=True)
            return render(request, 'usac_networking/Admin/Usuario/list.html',{
                        'title':title,
                        'session'       : request.session['session'],
                        'usuarios'      : usuarios,
                        'catedraticos'  : catedraticos,
                        'administradores': administradores,
                        'msg'           : msg,
                        'err_msg'       : err_msg,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def UsuarioNueva(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            title = 'Usac NetWorking - Nueva Usuario'
            err_msg = ''
            msg = ''
            usuarioformset = modelformset_factory(Usuario, fields=('carnet','nombre','apellido','fotografia','correo','telefono','clave','catedratico','estado','rol',),widgets={"clave": forms.PasswordInput()})
            if request.method == "POST":
                usuarioform = usuarioformset(request.POST,request.FILES)
                if usuarioform.is_valid():
                    usuarioform.save()
                    msg = 'Usuario Creada.'
                    usuarios = Usuario.objects.filter(catedratico=False,rol=2)
                    administradores = Usuario.objects.filter(rol=1)
                    catedraticos = Usuario.objects.filter(catedratico=True)
                    return render(request, 'usac_networking/Admin/Usuario/list.html',{
                                'title':title,
                                'session'       : request.session['session'],
                                'usuarios'      : usuarios,
                                'catedraticos'  : catedraticos,
                                'administradores': administradores,
                                'msg'           : msg,
                                'err_msg'       : err_msg,
                                })
                else:
                    err_msg = usuarioform.errors
            else:
                usuarioform = usuarioformset(queryset=Usuario.objects.none())
            return render(request, 'usac_networking/Admin/Usuario/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : usuarioform,
                        'err_msg'       : err_msg,
                        'op'            : 'new'
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def UsuarioMod(request,u_id):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1 or int(request.session['user_id']) == int(u_id):
            title = 'Usac NetWorking - Modificar Usuario'
            err_msg = ''
            msg = ''
            usuarioformset = modelformset_factory(Usuario,  fields=('carnet','nombre','apellido','fotografia','correo','telefono','clave','catedratico','estado','rol',),max_num=1)
            if request.method == "POST":
                usuarioform = usuarioformset(request.POST,request.FILES)
                if usuarioform.is_valid():
                    usuarioform.save()
                    msg = 'Usuario Modificada.'
                    usuarios = Usuario.objects.get(catedratico=False,rol=2)
                    administradores = Usuario.objects.get(rol=1)
                    catedraticos = Usuario.objects.get(catedratico=True)
                    return render(request, 'usac_networking/Admin/Usuario/list.html',{
                                'title'         : title,
                                'session'       : request.session['session'],
                                'usuarios'      : usuarios,
                                'catedraticos'  : catedraticos,
                                'administradores': administradores,
                                'msg'           : msg,
                                'err_msg'       : err_msg
                                })
                else:
                    err_msg = usuarioform.errors
            else:
                usuarioform = usuarioformset(queryset=Usuario.objects.filter(pk=u_id))
            return render(request, 'usac_networking/Admin/Usuario/detail.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        'formset'       : usuarioform,
                        'err_msg'       : err_msg,
                        'op'            : 'upd',
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))

def UsuarioDetail(request,u_id):
    title = 'Usac NetWorking - Detalle Usuario'
    err_msg = ''
    msg = ''
    friend = 0 #no puedo visualizar niguna opcion
    usuario = Usuario.objects.get(pk=u_id)
    if request.session.get('log_in'):
        editable = usuario.id == request.session['user_id'] or request.session['log_in'] == 1
        if not editable:
            try:
                friendship = Relacion_Amistad.objects.get(origen=request.session['user_id'],relacion=u_id)
                if friendship.estado:
                    friend = 1 #existe una relacion de amistad, se puede eliminar
                else:
                    friend = 2#la solicitud esta pendiente de aprovacion
            except Relacion_Amistad.DoesNotExist:
                try:
                    friendship2 = Relacion_Amistad.objects.get(relacion=request.session['user_id'],origen=u_id)
                    if friendship2.estado:
                        friend = 1 #existe una relacion de amistad, se puede eliminar
                    else:
                        friend = 4#la solicitud esta pendiente de aprovacion, se puede rechazar o aceptar
                except Relacion_Amistad.DoesNotExist:
                    friend = 3#no existe una relacion de amistad, se puede mandar
    else:
        editable = False
    print friend
    return render(request, 'usac_networking/public/profile.html',{
                'title'         : title,
                'session'       : request.session['session'],
                'err_msg'       : err_msg,
                'usuario'       : usuario,
                'editable'      : editable,
                'friend'        : friend,
                })

def Usuarioprofile(request):
    if request.session.get('log_in'):
            title = 'Usac NetWorking - Perfil Usuario'
            err_msg = ''
            msg = ''
            usuario = Usuario.objects.get(pk=request.session['user_id'])

            usuarios    = list(Relacion_Amistad.objects.filter(origen=request.session['user_id']))
            temas       = list(Tema.objects.filter(usuario=request.session['user_id']))
            circulos    = list(Circulo.objects.filter(usuario=request.session['user_id']))

            usus_inv    = Relacion_Amistad.objects.filter(relacion=request.session['user_id'])
            temas_inv   = Usuario_inv.objects.filter(usuario=request.session['user_id'])
            circus_inv  = Circulo_descrp.objects.filter(usuario=request.session['user_id'])

            editable    = usuario.id == request.session['user_id'] or request.session['log_in'] == 1

            if request.session.get('session') is None:
                session = 0
            else:
                session = request.session['session']

            for tema_inv in temas_inv:
                temas.append(tema_inv.tema)
            for usu_inv in usus_inv:
                usuarios.append(usu_inv.origen)
            for circu_inv in circus_inv:
                circulos.append(circu_inv.circulo)

            return render(request, 'usac_networking/public/profile.html',{
                        'title'         : title,
                        'session'       : session,
                        'err_msg'       : err_msg,
                        'usuario'       : usuario,
                        'editable'      : editable,
                        'temas'         : temas,
                        'usuarios'      : usuarios,
                        'circulos'      : circulos,
                        })

    return HttpResponseRedirect(reverse('usac_networking:index'))

def Amistades(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Perfil Usuario'
            amistades = Relacion_Amistad.objects.filter(origen__pk=request.session['user_id'],estado=True);
            amigos = Relacion_Amistad.objects.filter(relacion__pk=request.session['user_id'],estado=True);
            solicitudes = Relacion_Amistad.objects.filter(relacion__pk=request.session['user_id'],estado=False);
            return render(request, 'usac_networking/User/Amistad/list.html',{
                        'title'         : title,
                        'session'       : request.session['log_in'],
                        'amistades'     : amistades,
                        'amigos'        : amigos,
                        'solicitudes'   : solicitudes,
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
