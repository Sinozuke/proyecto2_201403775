# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms
from django.core.mail import send_mail, BadHeaderError
from datetime import datetime, timedelta
import re
import string
import random

from usac_networking.models import Usuario, Validacion_Usuario, Estado

def forgotpass(request):
    err_msg = ''
    title = 'Forgot - Usac Networking'
    if request.session.get('log_in'):
        return HttpResponseRedirect(reverse('usac_networking:index'))
    if request.method == 'POST':
        if request.POST.get('user_carne') is None:
            err_msg = 'No ha identificado ningun numero de carnet'
        else:
            try:
                user = Usuario.objects.get(carnet=request.POST['user_carne'])
                var_cont = 'Usuario: ' + user.nombre + ' ' + user.apellido + '\n'
                var_cont += 'Carnet: ' + str(user.carnet) + '\n'
                var_cont += 'Correo: ' + user.correo + '\n'
                var_cont += u'Contraseña: ' + user.clave + '\n'
                send_mail('Correo de recuperacion de credenciales - Usac Networking', var_cont, 'songokusinozuke@gmail.com',[user.correo], fail_silently=False)
                return render(request, 'usac_networking/Frontend/forgot.html',{
                            'title':title,
                            'session' : 0,
                            'correo'  : user.correo[:4],
                            'err_msg' : err_msg,
                            })
            except Usuario.DoesNotExist:
                err_msg = 'No Existe ningun usuario con este numero de carnet'
        return render(request, 'usac_networking/Frontend/forgot.html',{
                    'title':title,
                    'session' : 0,
                    'err_msg' : err_msg,
                    })
    return render(request, 'usac_networking/Frontend/forgot.html',{
                'title':title,
                'session' : 0,
                'err_msg' : err_msg,
                })


def login(request):
    err_msg = ''
    if request.POST.get('user_carne'):
        if request.POST.get('password'):
            try:
                user = Usuario.objects.get(carnet=request.POST['user_carne'])
                if user.estado.pk == 1:
                    err_msg = 'La Cuenta se ha eliminado, contacte con el administrador de la pagina'
                elif user.estado.pk == 2:
                    err_msg = 'La Cuenta se encuentra aun sin confirmacion, confirme su cuenta en el link que se envio a su correo electronico. si aun presenta problemas contacte con el administrador de la paguina'
                elif user.estado.pk == 3:
                    err_msg = 'La Cuenta se encuentra suspendida, contacte con el administrador de la pagina'
                elif user.clave == request.POST['password']:
                    request.session['user_id'] = user.id
                    request.session['log_in'] = user.rol.pk
                    request.session.set_expiry(0)
                    if user.rol.pk == 1:
                        request.session['session'] = 1
                        return HttpResponseRedirect(reverse('usac_networking:Admin_index'))
                    if user.rol.pk == 2:
                        request.session['session'] = 2
                        return HttpResponseRedirect(reverse('usac_networking:User_index'))
                else:
                    err_msg = 'Contraseña incorrecta'
            except Usuario.DoesNotExist:
                err_msg = 'El Usuario no Existe'
    return render(request, 'usac_networking/Frontend/login.html',{
                'title'     : 'Login - Usac Networking',
                'err_msg'   : err_msg,
                'session'   : 0
            })

def pvalidate(request):
    return render(request, 'usac_networking/Frontend/pvalidate.html',{
                'title'     : 'Confirm - Usac Networking',
                'session'    : 0,
            })

def validate(request,val_id):
    resultado = -1
    try:
        validacion = Validacion_Usuario.objects.get(val=val_id)
        if not validacion.estado:
            if datetime.date(validacion.fecha) > datetime.date(datetime.now()):
                usuario = Usuario.objects.get(pk=validacion.usuario.pk)
                if usuario.estado.pk == 1: #Eliminado
                    resultado = -2
                elif usuario.estado.pk == 2: #No Confirmado
                    usuario.estado=Estado.objects.get(pk=4)
                    validacion.estado=True
                    usuario.save()
                    validacion.save()
                    resultado = 1
                elif usuario.estado.pk == 3: #Susopendida
                    resultado = -1
                elif usuario.estado.pk == 4: #Activo
                    resultado = 3
            else:
                    resultado = 2
        else:
            resultado = 3
        return render(request, 'usac_networking/Frontend/validate.html',{
                    'title'     : 'Confirm - Usac Networking',
                    'codigo'    : val_id,
                    'resultado' : resultado,
                    'session'    : 0,
                })
    except Validacion_Usuario.DoesNotExist:
        raise Http404("Invalid Keyword")

def sign_in(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 1:
            return HttpResponseRedirect(reverse('usac_networking:Admin_index'))
        if request.session['log_in'] == 2:
            return HttpResponseRedirect(reverse('usac_networking:User_index'))
    title = 'Usac NetWorking - Sign in'
    pattern = re.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).{8,}$")
    userformset = modelformset_factory(Usuario, exclude=('estado','rol',),widgets={"clave": forms.PasswordInput()})
    msg = ''
    if request.method == "POST":
        userform = userformset(request.POST,request.FILES)
        if userform.is_valid():
            if pattern.match(userform.cleaned_data[0]['clave']):
                try:
                    var_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(50))
                    nuevo_usuario = userform.save()
                    validation_log = Validacion_Usuario(
                        usuario=nuevo_usuario[0],
                        val=var_string,
                        fecha=datetime.now() + timedelta(days=1),
                        )
                    validation_log.save()
                    var_dir = '127.0.0.1:8000/usac_networking/validate/'+var_string+'/'
                    var_cont = 'Bienvenido \"'+nuevo_usuario[0].nombre+'\" ('+str(nuevo_usuario[0].carnet)+').\n\n el sistema de ayuda al estudiante de la facultada ingenieria (Usac Networking) te da la bienvenida.\n porfavor nesesitamos que confirmes tu cuenta en dando click en la siguiente direccion.\n\n\n\n'+var_dir+'\n\n\n'
                    send_mail('Correo de Confirmacion de Cuenta - Usac Networking', var_cont, 'songokusinozuke@gmail.com',[userform.cleaned_data[0]['correo']], fail_silently=False)
                    return HttpResponseRedirect(reverse('usac_networking:pvalidate'))
                except BadHeaderError:
                    msg = 'Invalid header found.'
            else:
                msg = 'La clave deve contener minimo 8 caracteres, 1 mayuscula, 1 numero y 1 simbolo'
        else:
            msg = userform.errors
    else:
        userform = userformset(queryset=Usuario.objects.none())
    return render(request, 'usac_networking/Frontend/sign_up.html',{
                'title'     : title,
                'session'   : 0,
                'formset'   : userform,
                'err_msg'   : msg
                })

def logout(request):
    try:
        del request.session['user_id']
        del request.session['session']
        request.session['log_in'] = 0
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('usac_networking:index'))
