# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms import modelformset_factory
from django.http import Http404
from django import forms

from usac_networking.models import Tema, Ciencia_rel, Facultad_rel, Carrera_rel, Usuario_inv, Respuesta

def Chat(request):
    if request.session.get('log_in'):
        if request.session['log_in'] == 2:
            title = 'Usac NetWorking - Chat'
            return render(request, 'usac_networking/User/Chat/list.html',{
                        'title'         : title,
                        'session'       : request.session['session'],
                        })
    return HttpResponseRedirect(reverse('usac_networking:index'))
