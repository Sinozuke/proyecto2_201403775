$(function(){


  repaint();

	// Delete buttons
	$('.notificacion').click(function(){
    var notificacion  = $(this).attr('data-id');
    var url_action    = $(this).attr('data-url');
    var op            = $(this).attr('data-op');
    $.ajax({
        url: url_action,
        data: {
            "id": notificacion,
            "op": op
        },
        cache: false,
        type: "GET",
        dataType: "json",
        success: function(response) {
          $('#notificacion'+notificacion).hide();
          switch (op) {
            case "aceptar":
              $('#notificacion_a'+notificacion).show();
              break;
            case "rechazar":
              $('#notificacion_e'+notificacion).show();
              break;
            case "salir":
              $('#circulo'+notificacion).hide()
              break;
          }
        },
        error: function(xhr) {
          console.log(response);
        }
    });
	});
});
