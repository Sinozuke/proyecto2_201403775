var running = false;
$(function(){
    $('.agregar_a').click(function(){
      if (!running) {
        $(this).hide();
        running = true;
        $.ajax({
            url: url_amistad,
            data: {
                "u1_id"       : user_id,
                "u2_id"       : user_id2,
                "op"          : "agregar"
            },
            cache: false,
            type: "GET",
            dataType: "json",
            success: function(response) {
              $('#cancelar_solicitud').show();
              alert("Solicitud de Amistad Enviada");
              running = false;
            },
            error: function(xhr) {
              $(this).show();
              console.log(xhr);
              running = false;
            }

        });
      }
    });

    $('.eliminar_a').click(function(){
      if (!running) {
        $(this).hide();
        running = true;
        $.ajax({
            url: url_amistad,
            data: {
                "u1_id"       : user_id,
                "u2_id"       : user_id2,
                "op"          : "cancelar"
            },
            cache: false,
            type: "GET",
            dataType: "json",
            success: function(response) {
              $('#mandar_solicitud').show();
              alert("Solicitud de Amistad Eliminada");
              running = false;
            },
            error: function(xhr) {
              $(this).show();
              console.log(xhr);
              running = false;
            }

        });
      }
    });

    $('.aceptar_a').click(function(){
      if (!running) {
        $('#solicitud'+$(this).attr('data-id')).hide();
        running = true;
        $.ajax({
            url: url_amistad,
            data: {
                "id"          : $(this).attr('data-id'),
                "op"          : "aceptar"
            },
            cache: false,
            type: "GET",
            dataType: "json",
            success: function(response) {
              running = false;
            },
            error: function(xhr) {
              $('#solicitud'+$(this).attr('data-id')).show();
              console.log(xhr);
              running = false;
            }

        });
      }
    });

    $('.rechazar_a').click(function(){
      if (!running) {
        $('#solicitud'+$(this).attr('data-id')).hide();
        running = true;
        $.ajax({
            url: url_amistad,
            data: {
                "id"          : $(this).attr('data-id'),
                "op"          : "rechazar"
            },
            cache: false,
            type: "GET",
            dataType: "json",
            success: function(response) {
              running = false;
            },
            error: function(xhr) {
              $('#solicitud'+$(this).attr('data-id')).show();
              console.log(xhr);
              running = false;
            }

        });
      }
    });

    $('.eliminar').click(function(){
      if (!running) {
        $('#amistad'+$(this).attr('data-id')).hide();
        running = true;
        $.ajax({
            url: url_amistad,
            data: {
                "id"          : $(this).attr('data-id'),
                "op"          : "eliminar"
            },
            cache: false,
            type: "GET",
            dataType: "json",
            success: function(response) {
              running = false;
            },
            error: function(xhr) {
              $('#amistad'+$(this).attr('data-id')).show();
              console.log(xhr);
              running = false;
            }

        });
      }
    });


});
