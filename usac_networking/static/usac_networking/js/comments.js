var running = false;

function delComment(){

    if (running) {
      return
    }

    running = true;

    var id = $(this).attr('data-id');
    var padre = $(this).attr('data-padre');
    var tipo = $(this).attr('data-tipo');

    $.ajax({
        url: url_comment_del,
        data: {
            "id"          : id,
            "tipo"        : tipo,
            "padre"       : padre
        },
        cache: false,
        type: "GET",
        dataType: "json",
        success: function(response) {
          $('#panel_comment'+id).hide();
          repaint();
          setting();
        },
        error: function(xhr) {
          console.log(xhr);
          running = false;
        }

    });

}

function comentar(){

  if (running) {
    return
  }

  running = true;

  var padre = $(this).attr('data-padre');
  var tipo = $(this).attr('data-tipo');
  var contenido = $('#textarea'+padre).val();
  $('#textarea'+padre).val("");

  $.ajax({
      url: url_comment,
      data: {
          "padre"       : padre,
          "tipo"        : tipo,
          "contenido"   : contenido,
          "user_id"     : user_id
      },
      cache: false,
      type: "GET",
      dataType: "json",
      success: function(response) {
        var result = JSON.parse(response['result']);
        var data = result[0]['fields'];
        var user = JSON.parse(response['usuario'])[0]['fields'];

        switch (tipo) {
          case "tema":
            $('#nuevo_comentario'+padre).append("<li id=\"panel_comment" + result[0]['pk'] + "\" class=\"list-group-item\">"
              +"<div class=\"panel panel-default\">"
                +"<div class=\"panel-heading\">"
                  + user['nombre'] + " " + user['apellido']
                  +"<small>" + user['carnet'] + "</small>"
                +"</div>"
                +"<div class=\"panel-body\">"
                  +"<p>" + data['contenido'] + "</p>"
                  +"<div id=\"comentario" + result[0]['pk'] + "\">"
                  +"</div>"
                  +"<div id=\"responder" + result[0]['pk'] + "\" class=\"not-visible\">"
                    +"<input id=\"textarea" + result[0]['pk'] + "\" type=\"textarea\" name=\"comentario\" value=\"\">"
                    +"<input class=\"comentar\" type=\"submit\" data-padre=\"" + response['result'][0]['pk'] + "\" data-tipo=\"comentario\" value=\"Comentar\">"
                  +"</div>"
                +"</div>"
                +"<div class=\"panel-footer\">"
                +"<div class=\"row\">"
                  +"<div class=\"col-sm-3\">"
                      + "<p>Publicado el: " + data['fecha'] + "</p>"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"responder\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Responder\">"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"respuestas\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Ver Respuestas (" + data['num_res'] + ")\">"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"eliminar\" data-padre=\"" + padre + "\" data-tipo=\"comentario\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Eliminar Comentario\">"
                  +"</div>"
                +"</div>"
                +"</div>"
              +"</div>"
            +"</li>");
            break;
          case "comentario":
            $('#comentario'+padre).append("<li id=\"panel_comment" + result[0]['pk'] + "\" class=\"list-group-item\">"
              +"<div class=\"panel panel-default\">"
                +"<div class=\"panel-heading\">"
                  + user['nombre'] + " " + user['apellido']
                  +"<small>" + user['carnet'] + "</small>"
                +"</div>"
                +"<div class=\"panel-body\">"
                  +"<p>" + data['contenido'] + "</p>"
                  +"<div id=\"comentario" + result[0]['pk'] + "\">"
                  +"</div>"
                  +"<div id=\"responder" + result[0]['pk'] + "\" class=\"not-visible\">"
                    +"<input id=\"textarea" + result[0]['pk'] + "\" type=\"textarea\" name=\"comentario\" value=\"\">"
                    +"<input class=\"comentar\" type=\"submit\" data-padre=\"" + result[0]['pk'] + "\" data-tipo=\"comentario\" value=\"Comentar\">"
                  +"</div>"
                +"</div>"
                +"<div class=\"panel-footer\">"
                  +"<div class=\"row\">"
                    +"<div class=\"col-sm-3\">"
                        + "<p>Publicado el: " + data['fecha'] + "</p>"
                    +"</div>"
                    +"<div class=\"col-sm-3\">"
                      +"<input class=\"responder\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Responder\">"
                    +"</div>"
                    +"<div class=\"col-sm-3\">"
                      +"<input class=\"respuestas\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Ver Respuestas (" + data['num_res'] + ")\">"
                    +"</div>"
                    +"<div class=\"col-sm-3\">"
                      +"<input class=\"eliminar\" data-padre=\"" + padre + "\"  data-tipo=\"comentario\" type=\"submit\" data-id=\"" + result[0]['pk'] + "\" value=\"Eliminar Comentario\">"
                    +"</div>"
                  +"</div>"
                +"</div>"
              +"</div>"
            +"</li>");
            $('#responder'+padre).hide();
            break;
        }
        repaint();
        setting();
      },
      error: function(xhr) {
        console.log(xhr);
        running = false;
      }

  });
}

function getComments(){

  if (running) {
    return
  }

  running = true;

  var padre = $(this).attr('data-id');
  $(this).hide();

  $.ajax({
      url: url_comments,
      data: {
          "padre"       : padre,
      },
      cache: false,
      type: "GET",
      dataType: "json",
      success: function(response) {
        var comentarios = JSON.parse(response['coments']);
        var usuarios = JSON.parse(response['usuarios']);
        var numero = response['num'];
        var string = "";
        for (var i = 0; i < numero; i++) {
          string = string +"<li id=\"panel_comment" + comentarios[i]['pk'] + "\" class=\"list-group-item\">"
            +"<div class=\"panel panel-default\">"
              +"<div class=\"panel-heading\">"
                + usuarios[i]['fields']['nombre'] + " " + usuarios[i]['fields']['apellido']
                +"<small>" + usuarios[i]['fields']['carnet'] + "</small>"
              +"</div>"
              +"<div class=\"panel-body\">"
                +"<p>" + comentarios[i]['fields']['contenido'] + "</p>"
                +"<div id=\"comentario" + comentarios[i]['pk'] + "\">"
                +"</div>";
                if (comment) {
                  string = string+"<div id=\"responder" + comentarios[i]['pk'] + "\" class=\"not-visible\">"
                    +"<input id=\"textarea" + comentarios[i]['pk'] + "\" type=\"textarea\" name=\"comentario\" value=\"\">"
                    +"<input class=\"comentar\" type=\"submit\" data-padre=\"" + comentarios[i]['pk'] + "\" data-tipo=\"comentario\" value=\"Comentar\">"
                  +"</div>";
                }
              string = string+"</div>"
              +"<div class=\"panel-footer\">";
            if (comment_editable) {
              string = string
                +"<div class=\"row\">"
                  +"<div class=\"col-sm-3\">"
                      + "<p>Publicado el: " + comentarios[i]['fields']['fecha'] + "</p>"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"responder\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Responder\">"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"respuestas\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Ver Respuestas (" + comentarios[i]['fields']['num_res'] + ")\">"
                  +"</div>"
                  +"<div class=\"col-sm-3\">"
                    +"<input class=\"eliminar\" data-padre=\"" + padre + "\" data-tipo=\"comentario\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Eliminar Comentario\">"
                  +"</div>"
                +"</div>";
            }else {
              if (user_id === usuarios[i]['fields']['apellido']['pk']) {
                  string = string
                    +"<div class=\"row\">"
                      +"<div class=\"col-sm-3\">"
                          + "<p>Publicado el: " + comentarios[i]['fields']['fecha'] + "</p>"
                      +"</div>"
                      +"<div class=\"col-sm-3\">"
                        +"<input class=\"responder\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Responder\">"
                      +"</div>"
                      +"<div class=\"col-sm-3\">"
                        +"<input class=\"respuestas\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Ver Respuestas (" + comentarios[i]['fields']['num_res'] + ")\">"
                      +"</div>"
                      +"<div class=\"col-sm-3\">"
                        +"<input class=\"eliminar\" data-padre=\"" + padre + "\" data-tipo=\"comentario\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Eliminar Comentario\">"
                      +"</div>"
                    +"</div>";
              }else {
                string = string
                  +"<div class=\"row\">"
                    +"<div class=\"col-sm-4\">"
                        + "<p>Publicado el: " + comentarios[i]['fields']['fecha'] + "</p>"
                    +"</div>"
                    +"<div class=\"col-sm-4\">";
                    if (comment) {
                      string = string+"<input class=\"responder\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Responder\">";
                    }
                    string = string+"</div>"
                    +"<div class=\"col-sm-4\">"
                      +"<input class=\"respuestas\" type=\"submit\" data-id=\"" + comentarios[i]['pk'] + "\" value=\"Ver Respuestas (" + comentarios[i]['fields']['num_res'] + ")\">"
                    +"</div>"
                  +"</div>";
              }
            }
          string = string +"</div>"
            +"</div>"
          +"</li>";
          $('#comentario'+padre).append(string);
        }
        repaint();
        setting();
      },
      error: function(xhr) {
        console.log(xhr);
        running = false;
      }

  });
}

function setting(){

  $('.responder').click(function(){
    $('#responder'+$(this).attr('data-id')).show();
  });

  $('.comentar').click(comentar);

  $('.respuestas').click(getComments);

  $('.eliminar').click(delComment);

  running = false;

}

$(function(){

  repaint();
  setting();

});
