function repaint(){
  $('input').addClass('form-control');
  $('textarea').addClass('form-control');
  $('input[type=file]').addClass('form-control');
  $('input[type=checkbox]').removeClass('form-control');
  $('input[type=file]').removeClass('form-control');
}

$(document).ready(function(){

      repaint();

});
