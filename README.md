# README #

La Universidad de San Carlos de Guatemala ve la necesidad de integrar la
sociedad universitaria de manera que las diferentes facultades y escuelas sean
de un mismo equipo. Esto surge del análisis en el que se tomó en consideración
que estudiantes de diferentes facultades pueden ser de ayuda mutua entre otros
estudiantes de otras facultades, por ejemplo: un estudiante de ingeniería puede
resolver fácilmente un problema de matemáticas que se vea en la facultad de
farmacia, y viceversa dependiendo de los temas y la especialidad de las
facultades. Otro caso puede ser que un estudiante o catedrático de derecho, de
auditoría, de economía, etc., opine y sugiera sobre algún sistema que un
estudiante de ingeniería en sistemas está construyendo.

De esta manera surge la idea de construir un sistema el cual se llamará USAC-
NETWORKING el cual funcionará como una red social entre los estudiantes
que estén inscritos en la universidad, y estará enfocado con la visión de integrar
a los estudiantes para que sean de apoyo mutuo en diferentes aspectos
funcionando como una plataforma en el que se puedan crear temas y discutir
sobre ello.

python 2.7
django 1.8
cx_oracle
oracle XE11g